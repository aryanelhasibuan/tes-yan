resource "aws_vpc" "this" {
  cidr_block             = var.cidr_block
  enable_dns_hostnames    = true
  enable_dns_support      = true
  tags = {
     Name = "${var.project}-vpc"
   }
  }
resource "aws_subnet" "public" {
  count = length(var.public_subnets)
  vpc_id                  = aws_vpc.this.id
  cidr_block              = element(concat(var.public_subnets, [""]),count.index)
  availability_zone       = element(concat(var.azs, [""]), count.index)
  map_public_ip_on_launch = true
  tags = {
     Name = "${var.project}-public-${count.index}"
   }
 }
resource "aws_subnet" "private" {
  count = length(var.private_subnets)
  vpc_id                  = aws_vpc.this.id
  cidr_block              = element(concat(var.private_subnets, [""]),count.index)
  availability_zone       = element(concat(var.azs, [""]), count.index)
  map_public_ip_on_launch = false
  tags = {
     Name = "${var.project}-private-${count.index}"
   }
 }
resource "aws_route_table" "public"{
  vpc_id = aws_vpc.this.id
  tags   = {
      Name  = "${var.project}-rt-public"
   }
}
resource "aws_route" "public_igw_route" {
  route_table_id           = aws_route_table.public.id
  destination_cidr_block   = "0.0.0.0/0"
  gateway_id               = aws_internet_gateway.this.id
  timeouts {
     create = "5m"
  }
}
resource "aws_route_table" "private"{
  vpc_id = aws_vpc.this.id
   count = length(var.private_subnets)
  #subnet_id = element(aws_subnet.public.*.id, count.index)
  route {
    
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = element(aws_nat_gateway.NAT-Gateway.*.id, count.index)
  }
  tags   = {
      Name  = "${var.project}-rt-private"
   }
}
resource "aws_route_table_association" "public"{
  count = length(var.public_subnets)  
  subnet_id                = element(aws_subnet.public.*.id, count.index)
  route_table_id           = aws_route_table.public.id
}
resource "aws_route_table_association" "private"{
  count = length(var.private_subnets)  
  subnet_id                = element(aws_subnet.private.*.id, count.index)
  route_table_id           = element(aws_route_table.private.*.id, count.index)
}
resource "aws_internet_gateway" "this" {
 vpc_id = aws_vpc.this.id
 tags   = {
    Name = "${var.project}-igw"
  }
}
resource "aws_eip" "Nat-Gateway-EIP" {
  depends_on = [  
    aws_route_table_association.public
  ]
  vpc = true
}
resource "aws_nat_gateway""NAT-Gateway"{
# Allocating the Elastic IP to the NAT Gateway!
  count = length(var.public_subnets)
  allocation_id = aws_eip.Nat-Gateway-EIP.id
  # Associating it in the Public Subnet!
  subnet_id = element(aws_subnet.public.*.id, count.index)
  tags = {
    Name = "${var.project}-NAT"
  }
}
resource "aws_route_table" "NAT-Gateway-RT" {
  vpc_id = aws_vpc.this.id
  count = length(var.public_subnets)
  #subnet_id = element(aws_subnet.public.*.id, count.index)
  route {
    
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = element(aws_nat_gateway.NAT-Gateway.*.id, count.index)
  }
  tags = {
    Name = "Route Table for NAT Gateway"
  }
}
#public sg => uncomment if you need it
/*resource "aws_security_group""default_public" {
  name        = "${var.project}_default_public_sg"
  description = "${var.project} default public_sg"
  vpc_id      = aws_vpc.this.id
  ingress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks  = ["0.0.0.0/0"]#var.vpn_cidr_block
  description = "VPN IP"
  }
  ingress {
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks  = ["0.0.0.0/0"]
  description = "public access"
  }
  egress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name      = "${var.project}_public_sg"
  }
} */
resource "aws_security_group""default_private" {
 name        = "${var.project}_default_private_sg"
  description = "${var.project} default private _sg"
  vpc_id      = aws_vpc.this.id
  ingress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks  =[aws_vpc.this.cidr_block]
  description = "VPC "
  }
  ingress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]#var.vpn_cidr_block
  description = "VPN IP"
 }
  egress {
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name      = "${var.project}_private_sg"
  }
 }
#common ec2 instance => private or public
/*resource "aws_instance" "private-yan" {
  count                       = length(var.private_subnets)
  ami                         = "ami-066e9187daf1e6c5c"
  instance_type               = "t2.micro"
  key_name                    = var.key_name
  associate_public_ip_address = false
  security_groups             = [aws_security_group.default_private.id]
  subnet_id                   = element(aws_subnet.private.*.id, count.index)
  tags = {
    Name        = "${var.project}-private-yan"
    Description = "BCP instance Managed by terraform"
  }
}
/*resource "aws_instance" "public-yan" {
  count                       = length(var.public_subnets)
  ami                         = "ami-066e9187daf1e6c5c"
  instance_type               = "t2.micro"
  key_name                    = var.key_name
  associate_public_ip_address = true
  security_groups             = [aws_security_group.default_public.id]
  subnet_id                   = element(aws_subnet.public.*.id, count.index)
  tags = {
    Name        =  "${var.project}-public-yan"
    Description = "BCP instance Managed by terraform"
  }
}*/
