output "vpc_id" {
  description = "VPC ID"
  value       = aws_vpc.this.id
}
output "public_subnet_0"{
  description = "Public subnet 1"
  value       = aws_subnet.public[0].id
}
output "private_subnet_0"{
  description = "Private subnet 1"
  value       = aws_subnet.private[0].id
}
output "private_security_group"{
  description = "Private Subnet Security Group"
  value       = aws_security_group.default_private.id
}
/*output "private_subnet_1"{
  description = "Private subnet 2"
  value       = aws_subnet.private[0].id
}
output "public_security_group"{
  description = "Public Subnet Security Group"
  value       = aws_security_group.default_public.id
}*/
