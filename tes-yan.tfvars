project         = "tes"
region          = "ap-northeast-1"
cidr_block      = "10.0.0.0/16"
public_subnets  = ["10.0.1.0/24"]
private_subnets = ["10.0.2.0/24"]
azs             = ["ap-northeast-1a", "ap-northeast-1c"]
key_name        = "tes-yan"
ami             = "ami-07f65177cb990d65b"
