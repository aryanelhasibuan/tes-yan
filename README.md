# Tes Yan

Setup AWS infrastructure menggunakan terraform

Terdapat beberapa config yang dicomment, karena tidak dibutuhkan dalam case ini. Config yang dicomment, penulis gunakan dalam pengetesan setiap konfigurasi yang ditulis.

Terdapat file tfvars yang dapat digunakan sebagai inputan setiap variable. 

Untuk menjalankan dapat menggunakan command :
- terraform init
- terraform plan (jika memasukkan variable secara manual)
- terraform plan var-file=tes-yan.tfvars (menggunakan inputan yang sudah disediakan)
- terraform apply
- terraform apply var-file=tes-yan.tfvars 
- terraform destroy(
- terraform destroy var-file=tes-yan.tfvars 

Asumsi : Region dan access_key dan secret_key di store dalam local environment variabel
